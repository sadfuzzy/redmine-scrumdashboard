# scrumDashboard - Add scrum functionality to any Redmine installation
# Copyright (C) 2009 BrokenTeam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class DashboardController < ApplicationController
  unloadable

  def index
    get_globals
  end

  def settings
    @project = Project.find(params[:id])
    @dashboard = Dashboard.find_by_project_id(@project.id)
    @version = params[:version] ? Version.find(params[:version]) : @project.versions.sort.reverse.first
    @all_trackers = Tracker.all
    @all_statuses = IssueStatus.all.sort! { |a, b| a.position <=> b.position }
    @project_trackers = @dashboard.project_trackers
    @project_statuses = @dashboard.project_statuses

  rescue ActiveRecord::RecordNotFound
    render_404
  end

  # Update status of a selected issue.
  def update
    issue = Issue.find(params[:id].split("-")[2])
    old_status = issue.status
    dashboard = Dashboard.find_by_project_id(issue.project.id)
    version = Version.find_by_id(params[:version])
    drop = params[:id].split("-")[1]
    col = params[:id].split("-")[3]
    filter = params[:id].split("-")[4]
    where = params[:where]
    requested_status = IssueStatus.find_by_id(where)
    allowed_statuses = issue.new_statuses_allowed_to(User.current)

    # Check that the user is allowed to update the status of the issue.
    # and if issue dropped on original area, ignore it. (prevent dirty history)
    if (allowed_statuses.include? requested_status) && (requested_status != old_status)
      issue.update_attribute(:status_id, where)
      # Update the journal containing all the changes to the issue.
      journal = issue.init_journal(User.current)
      journal.details << JournalDetail.new(:property => 'attr',
                                           :prop_key => 'status_id',
                                           :old_value => old_status.id,
                                           :value => where)
      journal.save

    else
      # The user is not allowed to update the status. Override the requested status with the original.
      where = issue.status_id.to_s
      requested_status = IssueStatus.find_by_id(where)
    end
    swimline = Swimline.new(Issue.find(drop), dashboard, version)
    column = swimline.column_by_status(requested_status)

    render :update do |page|
      height = swimline.height(filter) * dashboard.line_height
      last = column.status == dashboard.project_statuses.last
      # Remove the issue from the dashboard and draw it to the new status.
      page.replace params[:id], ""
      page.replace_html("drop-"+drop+"-"+where,
                        draw_content(column, col, 100, height, dashboard.maintrackers, last, filter))
      # Adjust the height of the frame to fit the new placement of issues.
      dashboard.project_statuses.each do |status|
        page << "$('drop-"+drop+"-"+status.id.to_s+"').morph('height: "+height.to_s+"px;');"
      end
    end
  end

  def update_settings
    @dashboard = Dashboard.find(params[:id])
    @dashboard.update_attributes(params[:dashboard])

    new = params[:new_ids] ? params[:new_ids] : Array.new
    old = params[:old_ids] ? params[:old_ids] : Array.new
    new_ids = Array.new(new-old)
    old_ids = Array.new(old-new)

    if params[:change] == "Status" && old_ids.include?(session[:sort][:status])
      session[:sort] = { :status => nil, :reverse => nil }
    end

    # Update trackers and statuses included on the dashboard.
    old_ids.each do |oid|
      if params[:change] == "Tracker"
        dtracker = DashboardTracker.where(["dashboard_id = ? AND tracker_id = ?", @dashboard.id, oid.to_i]).first
        DashboardTracker.delete(dtracker.id)
      else
        dstatus = DashboardStatus.where(["dashboard_id = ? AND status_id = ?", @dashboard.id, oid.to_i]).first
        DashboardStatus.delete(dstatus.id)
      end
    end

    new_ids.each do |nid|
      if params[:change] == "Tracker"
        puts "creating tracker with db_id #{@dashboard.id} and nid #{nid}"
        DashboardTracker.create!(:dashboard_id => @dashboard.id, :tracker_id => nid.to_i)
      else
        puts "creating status with db_id #{@dashboard.id} and nid #{nid}"
        DashboardStatus.create(:dashboard_id => @dashboard.id, :status_id => nid.to_i)
      end
    end

    # Update tracker colors and maintrackers
    if params[:change] == "Tracker"
      maintrackers_new = params[:tracker_ids] ? params[:tracker_ids] : Array.new
      maintrackers_old = params[:old_tracker_ids] ? params[:old_tracker_ids] : Array.new
      new_ids = Array.new(maintrackers_new-maintrackers_old)
      old_ids = Array.new(maintrackers_old-maintrackers_new)

      old_ids.each do |oid|
        dtracker = DashboardTracker.where(["dashboard_id = ? AND tracker_id = ?", @dashboard.id, oid.to_i]).first

        if dtracker
          dtracker.maintracker = 0
          dtracker.save!
        end
      end

      new_ids.each do |nid|
        dtracker = DashboardTracker.where(["dashboard_id = ? AND tracker_id = ?", @dashboard.id, nid.to_i]).first

        if dtracker
          dtracker.maintracker = 1
          dtracker.save!
        end
      end

      if params[:textcolor]
        params[:textcolor].each do |key, value|
          if params[:new_ids].include?(key)
            puts "@dashboard.id : #{@dashboard.id}   tracker_id : #{key.to_i}"
            dtracker = DashboardTracker.where(["dashboard_id = ? AND tracker_id = ?", @dashboard.id, key.to_i]).first

            if dtracker
              dtracker.textcolor = value != "" ? value : dtracker.textcolor = "#2A5685"
              dtracker.save!
            end
          end
        end
      end

      if params[:bgcolor]
        params[:bgcolor].each do |key, value|
          if params[:new_ids].include?(key)
            dtracker = DashboardTracker.where(["dashboard_id = ? AND tracker_id = ?", @dashboard.id, key.to_i]).first
            if dtracker
              dtracker.bgcolor = value != "" ? value : dtracker.bgcolor = "yellow"
              dtracker.save!
            end
          end
        end
      end
    end

    flash[:notice] = l(:notice_successful_update)
    redirect_to :action => 'settings', :id => @dashboard.project, :tab => params[:change], :version => params[:version]
  end

  def update_selection
    get_globals
    render :partial => '/dashboard/dashboard'
  end

  # Visualise the user's workflow during dragging.
  def visualise_workflow
    # We decided to not use any more time trying to get this to work with IE since our customer didn't prioritize it.
    # The reason this doesnt work in IE is because of a bug with z-index.
    unless request.user_agent.index('MSIE')
      issue = Issue.find(params[:issue].split("-")[2])
      parent = Issue.find(params[:issue].split("-")[1])
      dashboard = Dashboard.find_by_project_id(issue.project.id)
      # The original background color of the status.
      returncolor = params[:issue].split("-")[3] == "1" ? "#EEE" : "#FFF"
      project_statuses = dashboard.project_statuses
      allowed_statuses = issue.new_statuses_allowed_to(User.current) & project_statuses

      js_response = ""
      project_statuses.each do |s|
        # Make sure that only the allowed statuses are visualised.
        if s == allowed_statuses[0]
          bgcolor = params[:do] ? "#C6EAC3" : returncolor
          allowed_statuses.shift
        else
          bgcolor = returncolor
        end
        # Change the background color of the selected status to visualise the workflow.
        js_response << "$('#drop-#{parent.id}-#{s.id}').css('background-color','#{bgcolor}');"
      end

      respond_to do |format|
        format.js { render :js => js_response }
      end
    end
  end

  private
  def get_globals
    if session[:sort].nil? then
      session[:sort] = { :status => nil, :reverse => nil }
    end

    if params[:sort] then
      session[:sort][:status] = params[:sort]
    end
    if params[:reverse] then
      session[:sort][:reverse] = params[:reverse].to_i
    end
    @project = Project.find(params[:id])
    @sort = session[:sort][:status]
    @reverse = session[:sort][:reverse]

    @dashboard = Dashboard.find_by_project_id(@project.id)
    @dashboard = Dashboard.create(:project_id => @project.id) unless @dashboard

    @issuestatuses = @dashboard.project_statuses
    @versions = @project.versions.sort
    @version = params[:version_id] ? Version.find(params[:version_id]) : @versions.reverse.first

    if @version.nil?
      flash[:error] = "Please add a version to your Project!"
      redirect_to "/projects/#{@project.id}/settings/versions"
    end

    @filter = params[:filter] ? params[:filter] : "all"
    @swimlines = @sort ? @dashboard.swimlines(@version, IssueStatus.find(@sort), @filter) : @dashboard.swimlines(@version)
    if @reverse == 1 && !@swimlines.nil? then
      @swimlines.reverse!
    end

  rescue ActiveRecord::RecordNotFound
    render_404
  end
end
