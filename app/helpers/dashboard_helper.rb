# scrumDashboard - Add scrum functionality to any Redmine installation
# Copyright (C) 2009 BrokenTeam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

module DashboardHelper

  def draw_columns(issuestatuses, col_max_length, sort = nil, reverse = nil)
    columnlength = 99 / issuestatuses.length
    columnlength = columnlength >= col_max_length ? col_max_length : columnlength
    html_class = "header"
    html_style = "width:#{columnlength}%;"
    url_options = params
    url_options[:action] = 'update_selection'
    i = 0

    html = ""

    issuestatuses.each do |status|
      html_style += "border-right: 1px solid #999;" if i != (issuestatuses.length - 1)

      url_options[:sort] = status.id
      url_options[:reverse] = (sort == status.id.to_s && reverse == 0) ? 1 : 0

      image = image_path('sort_asc.png')
      
      if sort && reverse == 0        
        image = image_path('sort_desc.png')   
      end

      html += content_tag :div, class: html_class, style: html_style do
        status.name.html_safe + ' ' + image_tag(image)
      end.html_safe

      i += 1
    end

    html.html_safe
  end

  def draw_issue(issue, parent_id, maintrackers, col, dashboard, filter)
    allowed_statuses = issue.new_statuses_allowed_to(User.current) & dashboard.project_statuses - [issue.status]
    draggable = allowed_statuses.length > 0
    dbtracker = DashboardTracker.where(["dashboard_id = ? AND tracker_id = ?", dashboard.id, issue.tracker.id]).first
    divclass = draggable ? 'draggable' : ''

    html_class = "db_issue #{divclass}"
    html_style = "background-color:white;border-left:6px solid #{dbtracker.bgcolor};"
    html_style += "width:#{dashboard.issue_width}%;height:#{dashboard.line_height-10}px"
    html_id = "issue-#{parent_id}-#{issue.id}-#{col}-#{filter}"

    # Display tooltip.
    issue_subject = issue.subject.gsub(/"/, '\\\\"').gsub(/'/, '`')
    html_onmouseover = "tooltip(\"<b>#{issue_subject}</b>"

    if issue.description
      issue_description = issue.description.gsub(/\r\n/, "<br/>").gsub(/"/, '\\\\"').gsub(/'/, '`')
      html_onmouseover += "<br/>#{issue_description}"
    end

    html_onmouseover += "<br/><b>#{l(:field_start_date)}:</b> #{format_date(issue.start_date)}<br/>" +
        "<b>#{l(:field_due_date)}:</b> #{format_date(issue.due_date)}<br/>" +
        "<b>#{l(:field_assigned_to)}:</b> #{issue.assigned_to}<br/>" +
        "<b>#{l(:field_priority)}:</b> #{issue.priority.name}\");"
    html_onmouseout = "closetooltip();"

    content_tag :div, id: html_id, class: html_class, style: html_style, onmouseover: html_onmouseover, onmouseout: html_onmouseout do
      link_to "#<strong>#{issue.id}</strong>: #{issue.subject}".html_safe, issue_path(issue),
              { :style => "color:#{dbtracker.textcolor}", :onmousedown => "wasdragged = false;",
                :onmouseup => "if(wasdragged){this.href='javascript:void(0)';}" }
    end
  end

  def draw_content(column, col, length, height, maintrackers, last = nil, filter = nil)
    issue = column.swimline.main_issue
    dashboard = column.swimline.dashboard

    divclass = (col == 1) ? 'content-1' : 'content-2'
    divclass += 'b' if request.user_agent.index('MSIE')

    style = "width:#{length}%;height:#{height}px;"
    style += "border-right: 1px solid #999;" unless last
    id = "drop-#{issue.id.to_s}-#{column.status.id.to_s}"

    content_tag :div, class: "column #{divclass}", style: style, id: id do
      #puts drop_receiving_element id, :url => { :action => "update",
      #                                     :where => column.status.id.to_s,
      #                                     :drop => issue.id.to_s,
      #                                     :version => @version },
      #                       :hoverclass => "hover",
      #                       :before => "Element.hide(element)"
      jscc = "$('.droppable').droppable({
          hoverclass:'hover',
          drop:function(element) {
            $(element).hide();
            $.ajax({
                       url: '#{dashboard_update_path({drop: issue.id, version: @version.id, where: column.status.id})}'
                   },
                   {async:true,
                    dataType:'string',
                    parameters:'id=' + encodeURIComponent(element.id)
                   })
          }
      });"
      html = javascript_tag jscc

      column.issues.each do |i|
        if filter != "mine" || i.assigned_to == User.current
          html << draw_issue(i, issue.id, maintrackers, col, dashboard, filter)
        end
      end

      html
    end
  end

  def draw_frame(issue, &block)
    html_start = "<div class='frame' style='clear:both;' id='frame-#{issue.id}'>"
    html_end = "</div>"

    html = html_start + capture(&block) + html_end
    concat(html)
  end

  def add_observer
    javascript_tag "$ ('.draggable').draggable({
       start: function() {
         var content = draggable.element.id;
         var url = '/dashboard/visualise_workflow?do=true&issue='+encodeURIComponent(content);
         $.ajax(url, { asynchronous:true, evalScripts:true, method:'get' });
       },
       drag: function() {
         closetooltip(); wasdragged = true;
       },
       stop: function() {
         var content = draggable.element.id;
         var url = '/dashboard/visualise_workflow?issue='+encodeURIComponent(content);
         $.ajax(url, { asynchronous:true, evalScripts:true, method:'get' });
       }
    });"
  end

  def administration_settings_tabs
    [{ :name => 'Tracker', :partial => 'tracker', :label => 'label_tracker' },
     { :name => 'Status', :partial => 'status', :label => 'label_dashboard_status' },
     { :name => 'Visualization', :partial => 'visualization', :label => 'label_dashboard_visualization' }]
  end

  def default_colors
    [{ :name => l(:dashboard_color_custom), :bgcolor => '', :textcolor => '' },
     { :name => l(:dashboard_color_yellow), :bgcolor => '#FFFF00', :textcolor => '#2A5685' },
     { :name => l(:dashboard_color_white), :bgcolor => '#FFFFFF', :textcolor => '#2A5685' },
     { :name => l(:dashboard_color_khaki), :bgcolor => '#F0E68C', :textcolor => '#000000' },
     { :name => l(:dashboard_color_lawngreen), :bgcolor => '#7CFC00', :textcolor => '#000000' },
     { :name => l(:dashboard_color_sgilightblue), :bgcolor => '#7D9EC0', :textcolor => '#000000' },
     { :name => l(:dashboard_color_black), :bgcolor => '#000000', :textcolor => '#00FF00' },
     { :name => l(:dashboard_color_lightcoral), :bgcolor => '#F08080', :textcolor => '#000000' },
     { :name => l(:dashboard_color_skyblue), :bgcolor => '#87CEEB', :textcolor => '#000000' },
     { :name => l(:dashboard_color_lightsteelblue), :bgcolor => '#B0C4DE', :textcolor => '#000000' },
     { :name => l(:dashboard_color_yellowgreen), :bgcolor => '#9ACD32', :textcolor => '#000000' }]
  end
end
